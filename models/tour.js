const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TourSchema = new Schema({
	name: {
	type: String,
	required: [true, 'Tour name is required.']
	},
	description: {
	type: String,
	required: [true, 'Description is required.']
	},
	unitPrice: {
	type: Number,
	required: [true, 'Unit price is required.']
	},
	imageLocation: {
	type: String
	},
	categoryName: {
	type: String,
	required: [true, 'Category name is required.']
	},
	isArchived: {
	type: Boolean,
	required: [true, 'Archive status is required.']
	}

})

const Tour = mongoose.model('tour', TourSchema)

module.exports = Tour