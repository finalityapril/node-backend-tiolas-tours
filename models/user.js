const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
	name: {
		type: String,
		required: [true, 'Name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	password: {
		type: String,
		required: [true, 'Password is required']
	},
	role: {
		type: String,
		default: 'customer'
	},
	stripeCustomerId: {
		type: String,
		required: [true, 'Stripe customer ID is required.']
	},
	nowBooking: {
		type: String,
		default: 'false'
	},
	orders: [
		{
			datetimeRecorded: {
				type: Date,
				default: new Date()
			},
			paymentMode: {
				type: String,
				required: [true, 'Payment mode is required.']
			},
			totalPrice: {
				type: Number,
				required: [true, 'Total price is required.']
			},
			stripeChargeId: {
				type: String,
				default: null
			},
			tours: [
				{
					name: {
						type:String,
						required: [true, 'Item name is required.']
					},
					category : {
						type: String,
						required: [true, 'Category is required.']
					},
					quantity: {
						type: Number,
						required: [true, 'Quantity is required.']
					},
					bookDate: {
						type: Date,
						default: new Date()
					},
					unitPrice: {
						type: Number,
						required: [true, 'Unit price is required.']
					}
				}
			]
		}
	]
}, {
	timestamps: true
})

const User = mongoose.model('user', UserSchema)

module.exports = User