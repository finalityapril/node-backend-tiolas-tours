//Declare constants

const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const app = express()
const defaultPort = 4000
const designatedPort = process.env.PORT || defaultPort


//Declare middlewares
app.use(bodyParser.json())


//Enable Cross Origin Resource String

app.use(function(req, res, next) {
	res.header('Access-Control-Allow-Origin','*')
	res.header('Access-Control-Allow-Headers','Origin, X-Requested-With, Content-Type, Accept, Authorization')
	res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
	next()
})


mongoose.set('useFindAndModify', false);

//Declare database connection

 // mongoose.connect('mongodb://localhost/tours_tiolas', {
 // 	urlNewUrlParser: true,
 // 	useCreateIndex: true
 // })



const databaseUrl = process.env.DATABASE_URL || secret

mongoose.connect(databaseUrl, { useNewUrlParser:true, useCreateIndex: true })
mongoose.connection.once('open', () => {
console.log('Connection to MongoDB Atlas has been successfully tested.')
}).catch(function(err) {
console.log(err)
})

//Declare routes.

app.use('/api',require('./routes'))



//listen to server requests.

app.listen(designatedPort, () => {
	console.log('Node.js backend now online in port' + designatedPort)
})




// app.get('/', (req,res)=> {
// 	console.log('GET request detected on /path.')
// 	res.send('GET request detected on / path.')
// })