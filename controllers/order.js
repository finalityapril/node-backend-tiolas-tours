const User = require('../models/user')
const auth = require('../jwt-auth')

module.exports.user = (req, res) => {
	let condition = {
		_id: auth.getId(req.headers.authorization)
	}

	User.findOne(condition).exec().then((user) => {
        user.orders.sort(function(a,b)
            { 
                a = new Date(a.datetimeRecorded) 
                b = new Date(b.datetimeRecorded)
                return a>b ? -1 : a<b ? 1 : 0
            })                    
		res.status(200).json({ result: 'success', orders: user.orders })

	}).catch((err) => {
		res.status(400).json({ error: err.message })
	})
}



module.exports.admin = (req, res) => {
    
    let ordersArr = []
    let condition = {
        _id: auth.getId(req.headers.authorization)
    }
    User.find( condition[role="customer"] ).exec().then((user) => {
        for (let i = 0; i < user.length; i++) {
            let order = (user[i].orders)

            for (let j = 0; j < order.length; j++ ) {
            ordersArr.push(order[j])
            }
        }        
         ordersArr.sort(function(a,b)
            { 
               a = new Date(a.datetimeRecorded) 
               b = new Date(b.datetimeRecorded)
               return a>b ? -1 : a<b ? 1 : 0
            })                    
        res.status(200).json({ result: 'success', orders:ordersArr})
    }).catch((err) => {
        res.status(400).json({ error: err.message })
    })

}