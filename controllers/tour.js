const Tour = require('../models/tour')
const auth = require('../jwt-auth')


module.exports.all = (req, res) => {
	let condition = { isArchived: 0 } 
	
	Tour.find(condition).then((tours) => {
		res.status(200).json(tours)
	}).catch((err) => {
		res.status(400).json({error: err.message})
	})
}

module.exports.detail = (req, res) => {
	let condition = {_id: req.params._id}

	Tour.findOne(condition).exec().then((tour) => {
		res.status(200).json(tour)
	}).catch((err)=> {
		res.status(400).json({error: err.message})
	})
}

module.exports.new = (req, res) => {
	Tour.create(req.body).then((tour) => {
		res.status(200).json(tour)
	}).catch((err) => {
		res.status(400.).json({error: err.message})
	})
}

module.exports.update = (req, res) => {
	let searchParam = { _id: req.body._id }
	let updateParams = req.body

	Tour.findOneAndUpdate(searchParam, updateParams, (tour) => {
		res.status(200).json({ result: 'success'})
	}).catch((err) => {
		res.status(400).json({ error: err.message })
	})
}

module.exports.delete = (req, res) => {
	let searchParam = {_id: req.body._id}
	let updateParams = { isArchived: 1 }

	Tour.findOneAndUpdate(searchParam, updateParams, (tour) => {
		res.status(200).json({ result: 'success'})
	}).catch((err) => {
		res.status(400).json({ error: err.message })
	})
}