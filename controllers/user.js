const User = require('../models/user')
const bcrypt = require('bcrypt')
const stripe  = require('stripe')('sk_test_w17hLaGD8sNUk9qXXJKxvbbt00lM3h60cT')
const auth = require('../jwt-auth')

module.exports.login = (req, res) => {
	let condition = { email: req.body.email }
	User.findOne(condition).exec().then((user)=> {
		if(user == null) {
			res.status(200).json({ error: 'No user found, please try again.'})
		} else {
			bcrypt.compare(req.body.password, user.password, (err, result) => {
				if(err) { return res.status(400).json({ error: err.message })}

				if (result) {
					res.status(200).json({
						result: 'authenticated',
						role: user.role,
						name: user.name,
						token: auth.createToken(user),
						nowBooking: user.nowBooking
					})
				} else {
					res.status(400).json({ error: 'No result found.'})
				}
			})
		}
	}).catch((err) => {
		res.status(400).json({ error: err.message })
	})
}

module.exports.register = (req, res) => {
	bcrypt.hash(req.body.password, 10, (err,hash) => {
		
		if(err) { return res.status(400).json({ error:err.message }) }

			 stripe.customers.create({
			 	email: req.body.email,
			 	source: 'tok_mastercard'
			 }, (err, customer) => {
			 	if (err) { return res.status(400).json({ error: err.message}) }
				
				req.body.stripeCustomerId = customer.id
			 	req.body.password = hash

				User.create(req.body).then((user) => {
					res.status(200).json({ result: 'success'})
				}).catch((err) => {
					res.status(400).json({error: err.message})
				})

				
		})
	
	})
}

