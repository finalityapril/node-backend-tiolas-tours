const Tour = require('../models/tour')
const auth = require('../jwt-auth')
const User = require('../models/user')
const stripe  = require('stripe')('sk_test_w17hLaGD8sNUk9qXXJKxvbbt00lM3h60cT')

module.exports.info = (req, res) => {
	let cart = req.body
	let _ids = []

	for (let i=0; i<cart.length; i++) {
		_ids.push(cart[i]._id)
	}

	let condition = { '_id': { $in: _ids} }

	Tour.find(condition, (err,tours) => {
		if (err) { return res.status(400).json({error: err.message}) }
		// 	console.log(tours)
		// console.log(cart)

		let cartInfo = []

		for (let i = 0; i<tours.length; i++){
			for (let j=0; j<cart.length; j++) {
				if(tours[i]._id == cart[j]._id) {
					cartInfo.push({
						_id: tours[i]._id,
						name: tours[i].name,
						quantity: cart[j].quantity,
						unitPrice: tours[i].unitPrice,
						bookDate: cart[j].bookDate
					})
				}
			}
		}
		console.log(cartInfo)
		res.status(200).json(cartInfo)
	})
	
}


module.exports.checkout = (req, res) => {
	let userId = auth.getId(req.headers.authorization)
	let cart = JSON.parse(req.body.cart)
	let _ids = []
	let orderTours = []
	let totalPrice = 0

	for(let i = 0; i < cart.length; i++) {
		_ids.push(cart[i]._id)
	}

	let condition = {
		'_id': {
		$in: _ids
		}
	}

	Tour.find(condition, (err, tours) => {
		for(let i = 0; i < tours.length; i++) {
			for(let j = 0; j < cart.length; j++) {
				if(tours[i]._id == cart[j]._id) {
					let orderTour = {
						name: tours[i].name,
						category: tours[i].categoryName,
						quantity: cart[j].quantity,
						unitPrice: tours[i].unitPrice,
						bookDate: cart[j].bookDate
					}
					orderTours.push(orderTour)
					totalPrice += (parseFloat(cart[j].quantity) * parseFloat(tours[i].unitPrice))
				}
			}
		}

	
			User.findOne({ _id: userId }).exec().then((user) => {
				let newOrderDoc = user.orders.create({
					datetimeRecorded: new Date(),
					paymentMode: 'Cash on Delivery',
					totalPrice: totalPrice,
					tours: orderTours
					
				})
				
				user.orders.push(newOrderDoc)
				user.save()

				res.status(200).json({ result: 'success' })			
			}).catch((err) => {
				res.status(400).json({ error: err.message })
			})
		

	}).catch((err) => {
		res.status(400).json({ error: err.message })
	})

}

module.exports.CheckoutStripe = (req, res) => {
	let userId = auth.getId(req.headers.authorization)
	let cart = JSON.parse(req.body.cart)
	let _ids = []
	let orderTours = []
	let totalPrice = 0

	for(let i = 0; i < cart.length; i++) {
		_ids.push(cart[i]._id)
	}

	let condition = {
		'_id': {
		$in: _ids
		}
	}

	Tour.find(condition, (err, tours) => {
		for(let i = 0; i < tours.length; i++) {
			for(let j = 0; j < cart.length; j++) {
				if(tours[i]._id == cart[j]._id) {
					let orderTour = {
						name: tours[i].name,
						category: tours[i].categoryName,
						quantity: cart[j].quantity,
						unitPrice: tours[i].unitPrice,
						bookDate: cart[j].bookDate
					}
					orderTours.push(orderTour)
					totalPrice += (parseFloat(cart[j].quantity) * parseFloat(tours[i].unitPrice))
				}
			}
		}

		stripe.charges.create({
			amount: totalPrice * 100,
			description: 'Charge from Eyps\' Node.js',
			currency: 'php',
			customer: auth.getStripeCustomerId(req.headers.authorization)
		}, (err, charge) => {
			if(err) { return res.status(400).json({ error: err.message }) }

				User.findOne({ _id: userId }).exec().then((user) => {
				let newOrderDoc = user.orders.create({
					datetimeRecorded: new Date(),
					paymentMode: 'Stripe',
					stripeChargeId: charge.id,
					totalPrice: totalPrice,
					tours: orderTours
				})
				console.log(user)
				user.orders.push(newOrderDoc)
				user.save()

				res.status(200).json({ result: 'success' })			
			}).catch((err) => {
				res.status(400).json({ error: err.message })
			})			
		})

	}).catch((err) => {
		res.status(400).json({ error: err.message })
	})
}
