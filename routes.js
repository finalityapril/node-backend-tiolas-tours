//Declare constants.

const express = require('express')
const router = express.Router()
const auth = require('./jwt-auth')

const TourController = require('./controllers/tour')
const CategoryController = require('./controllers/category')
const UserController = require('./controllers/user')
const OrderController = require('./controllers/order')
const CartController = require('./controllers/cart')

//Route Declarations

//[SECTION] Routes for Tours
router.get('/tours', TourController.all)
router.get('/tour/:_id', TourController.detail)
router.post('/tour', auth.verify, TourController.new)
router.put('/tour', auth.verify, TourController.update)
router.delete('/tour', auth.verify, TourController.delete)

// [SECTION] Routes for Categories.
router.get('/categories', CategoryController.all)
router.get('/bookcategory', auth.verify, CategoryController.book)

// [SECTION] Routes for Orders.
router.get('/orders/user', auth.verify, OrderController.user)
router.get('/orders/admin', auth.verify, OrderController.admin)

// [SECTION] Routes for Users.
router.post('/user/login', UserController.login)
router.post('/user/register', UserController.register)

// [SECTION] Routes for Carts.
router.post('/cart/info', CartController.info)
router.post('/cart/checkout', auth.verify, CartController.checkout)
router.post('/cart/checkout-stripe', auth.verify, CartController.CheckoutStripe)


module.exports = router

